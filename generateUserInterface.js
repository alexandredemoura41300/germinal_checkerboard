const generateUserInterface = (width, height, black, white) => {
  let timesToRepeat;
  let isWidthOdd;
  let isReversed = false;

  if (width % 2 === 0) {
    timesToRepeat = width / 2;
    isWidthOdd = false;
  } else {
    timesToRepeat = (width - 1) / 2;
    isWidthOdd = true;
  }

  for (let i = 0; i < height; i++) {
    if (isWidthOdd) {
      console.log(`[${isReversed ? white : black}][${isReversed ? black : white}]`
      .repeat(timesToRepeat) + `[${isReversed ? white : black}]`);
      isReversed = !isReversed;
    } else {
      console.log(`[${isReversed ? white : black}][${isReversed ? black : white}]`.repeat(timesToRepeat));
      isReversed = !isReversed;
    }
  }
}

module.exports = generateUserInterface;