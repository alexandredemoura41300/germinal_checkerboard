const generateUserInterface = require("./generateUserInterface");

// Exemple: node index.js --width=28 --height=13 --black='g' --white='j'
// Arguments have default values if you don't fill them or fill them wrong
// width and height are expected to be numbers 
// black and white are expected to be a string of one character

const args = require('minimist')(process.argv.slice(2));

let width = 6;
if (args['width'] && typeof args['width'] === 'number') {
  width = args['width'];
}

let height = 6;
if (args['height'] && typeof args['height'] === 'number') {
  height = args['height'];
}

let black = 'x';
if (args['black'] && typeof args['black'] === 'string' && args['black'].length === 1) {
  black = args['black'];
}

let white = 'o';
if (args['white'] && typeof args['white'] === 'string' && args['white'] !== black && args['white'].length === 1) {
  white = args['white'];
}

generateUserInterface(width, height, black, white);