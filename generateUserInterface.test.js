const generateUserInterface = require('./generateUserInterface');

jest.spyOn(console, 'log');

test("generate the right checkerboard when width is even and make sure the next line is the reversed version of the previous one", () => {
  expect(console.log.mock.calls.length).toBe(0);

  generateUserInterface(4, 4, 'x', 'o');
  expect(console.log.mock.calls.length).toBe(4);
  expect(console.log.mock.calls[0][0]).toBe("[x][o][x][o]");
  expect(console.log.mock.calls[1][0]).toBe("[o][x][o][x]");
});

test("generate the right checkerboard when width is odd and make sure the next line is the reversed version of the previous one", () => {
  expect(console.log.mock.calls.length).toBe(0);

  generateUserInterface(5, 4, 'x', 'o');
  expect(console.log.mock.calls.length).toBe(4);
  expect(console.log.mock.calls[0][0]).toBe("[x][o][x][o][x]");
  expect(console.log.mock.calls[1][0]).toBe("[o][x][o][x][o]");
});

afterEach(() => {
  jest.clearAllMocks();
});